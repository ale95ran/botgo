﻿using BookTableBot.Utilities;
using FirstBotGo.Utilities;
using Google.Apis.Dialogflow.v2.Data;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;

namespace FirstBotGo.Controllers
{
    public class DialogFlowController : ApiController
    {
        public GoogleCloudDialogflowV2WebhookResponse Post(GoogleCloudDialogflowV2WebhookRequest obj)
        {
            var botModel = ModelMapper.DialogFlowToModel(obj);
            var botObject = obj.QueryResult;
            Console.WriteLine(botObject.QueryText);
            if (botModel == null || botObject ==null)
            {
                return null;
            }
            /*var client = new RestClient("https://chatbot.edge.com.py");
            // client.Authenticator = new HttpBasicAuthenticator(username, password);

            var request = new RestRequest("resource/{id}", Method.POST);
            request.AddParameter("name", "value"); 
            request.AddUrlSegment("id", "123"); 

            //request.AddHeader("header", "value");

            IRestResponse response = client.Execute(request);
            var content = response.Content;*/

            //botModel = IntentRouter.Process(botModel);
            botModel.Response.Text = "Hola soy Liza, en que puedo ayudarte";
            return ModelMapper.ModelToDialogFlow(botModel);
        }
        public string Get()
        {
            return "Hello DialogFlow!";
        }
    }
}